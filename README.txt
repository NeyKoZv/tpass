Projet à noter : VariableType

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)
Download the repository :

git clone git@framagit.org:NeyKoZv/tpass.git

Build the compiler and test it :

make test

Have a look at the output :

gedit test.s

Debug the executable :

ddd ./test

Test the executable : 

./test

Commit the new version :

git commit -a -m "What's new..."

Send to your framagit :

git push -u origin master

Get from your framagit :

git pull -u origin master


Program := [VarDeclarationPart] StatementPart
VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration}"."
VarDeclaration := Ident {";" Ident} ":" Type 
StatementPart := Statement {";" Statement} "."
Statement := AssignementStatement | For | Si | While | Switch | Display

AssignementStatement := Letter ":=" Expression
For := "for" expression uint64 "to"|"downto" expression bool "do" statement
Si := "if" expression bool "do" statement {"elsif" expression bool "do" statement} ["else" statement"]
While := "while" expression bool "do" statement
Switch := "switch" expression uint64 "case" expression uint "do" statement {"case" expression uint "do" statement} end
Diplay := "display" expression uint64

Expression := SimpleExpression [RelationalOperator SimpleExpression]
SimpleExpression := Term {AdditiveOperator Term}
Term := Factor {MultiplicativeOperator Factor}
Factor := Number | Letter | "(" Expression ")"| "!" Factor
Number := Digit{Digit}

AdditiveOperator := "+" | "-" | "||"
MultiplicativeOperator := "*" | "/" | "%" | "&&"
RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
Letter := "a"|...|"z"
Type := BOOLEAN | INTEGER


