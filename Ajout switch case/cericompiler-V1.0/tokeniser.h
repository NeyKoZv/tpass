// tokeniser.h : shared definition for tokeniser.l and compilateur.cpp

enum TOKEN {FEOF, UNKNOWN, NUMBER, KW, DISPLAY, ID, STRINGCONST, RBRACKET, LBRACKET, RPARENT, LPARENT, COMMA, 
SEMICOLON, DOT, ADDOP, MULOP, RELOP, NOT, ASSIGN};

