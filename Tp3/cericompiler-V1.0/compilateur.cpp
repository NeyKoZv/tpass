//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

void ReadToken(void){
	current=(TOKEN)lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
string Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return("UNSIGNED_INT");//----------------------------SET TYPE UNSIGNED INT -------------------
}

string Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return("UNSIGNED_INT"); //----------------------------SET TYPE UNSIGNED INT -------------------
}

string Expression(void);			// Called by Term() and calls Term()

string Factor(void){
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			return(Number());
	     	else
				if(current==ID)
					return(Identifier());
				else
					Error("'(' ou chiffre ou lettre attendue");
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
string Term(void){
	string type = "";
	OPMUL mulop;
	type = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		if(type != Factor())
			Error("Mauvais TYPE dans Term");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				type = "BOOLEAN"; //--------------------SET TYPE BOOLEAN-------------
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
string SimpleExpression(void){
	OPADD adop;
	string type = "";
	type = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		if(type != Term())
			Error("Mauvais type dans SimpleExpression");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				type = "BOOLEAN"; //-----------------------SET TYPE BOOLEAN-----------
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\""<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
string Expression(void){
	OPREL oprel;
	string type = "";
	type = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		if(type != SimpleExpression())
			Error("Mauvais type dans Expression");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		type = "BOOLEAN"; //------------------------------SET TYPE BOOLEAN---------------
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if("BOOLEAN" == Expression())
		Error("ASSIGNATION VARIABLE = BOOLEAN C'EST NON");
	cout << "\tpop "<<variable<<endl;
}

void Statement(void);

void ifStatement(void){
	int TagNbr = ++TagNumber;
	cout<<"si"<<TagNbr<<":"<<endl;
	ReadToken();
	if("BOOLEAN" != Expression())
		Error("Mauvais type test dans IF");
	cout<<"\tjz else"<<TagNbr<<endl;
	if(strcmp(lexer->YYText(),"then")==0){
		cout<<"then"<<TagNbr<<":"<<endl;
		ReadToken();
		Statement();
		cout<<"\tjmp finelse"<<TagNbr<<endl;
	cout<<"else"<<TagNbr<<":"<<endl;
	}
	else
		Error("Then attendu");
	if(strcmp(lexer->YYText(),"else")==0){
		ReadToken();
		Statement();
	}
	cout<<"finelse"<<TagNbr<<":"<<endl;
}

void whileStatement(void){
	int TagNbr = ++TagNumber;
	cout<<"while"<<TagNbr<<":"<<endl;
	ReadToken();
	if("BOOLEAN" != Expression())
		Error("MAUVAIS TYPE TEST DANS WHILE");
	cout<<"\tjz finwhile"<<TagNbr<<endl;
	if(strcmp(lexer->YYText(),"do")==0){
		cout<<"do"<<TagNbr<<":"<<endl;
		ReadToken();
		Statement();
		cout<<"\tjmp while"<<TagNbr<<endl;
	}
	else
		Error("do pour while attendu");
	cout<<"finwhile"<<TagNbr<<":"<<endl;
}

void forStatement(void){
	int TagNbr = ++TagNumber;
	string initFor = "";
	cout<<"for"<<TagNbr<<":"<<endl;
	ReadToken();
	//Il me faut la lettre d'assignation en mémoire
	initFor += lexer->YYText();
	AssignementStatement();
	if(strcmp(lexer->YYText(),"to")==0){
		cout<<"to"<<TagNbr<<":"<<endl;
		ReadToken();
		if("BOOLEAN"!=Expression())
			Error("MAUVAIS TYPE TEST DANS FOR");
		cout<<"\tjz finfor"<<TagNbr<<endl;
		if(strcmp(lexer->YYText(),"do")==0){
			cout<<"do"<<TagNbr<<":"<<endl;
			ReadToken();
			Statement();
			//Début incrémentation for
			cout<<"incrementation_for_debut"<<TagNbr<<":"<<endl;
			cout<<"\tpush "<<initFor<<endl;
			cout<<"\tpop %rcx"<<endl;
			cout<<"\taddq $1, %rcx"<<endl;
			cout<<"\tpush %rcx"<<endl;
			cout<<"\tpop "<<initFor<<endl;
			cout<<"fin_incrementation_for_debut"<<TagNbr<<":"<<endl;
			//fin incrémentation for
			cout<<"\tjmp to"<<TagNbr<<endl;
		}
		else
			Error("do pour for attendu");
		cout<<"finfor"<<TagNbr<<":"<<endl;
	}
	else
		Error("to pour for attendu");
}

void beginStatement(void){//Je sais pas quoi faire pour lui
	int TagNbr = ++TagNumber;
	cout<<"begin"<<TagNbr<<":"<<endl;
	ReadToken();
	Statement();
	while(strcmp(lexer->YYText(),"end")!=0){
		if(strcmp(lexer->YYText(),";")==0){
			ReadToken();
			Statement();
		}
		else
				Error("; attendu");
	}
	cout<<"end"<<TagNbr<<":"<<endl;
	ReadToken(); //J'ai eu end
}

void displayStatement(){
	ReadToken();
	if("UNSIGNED_INT" == Expression()){
		cout<<"\tpop %rdx"<<endl;
		cout<<"\tmovq $FormatString1, %rsi"<<endl;
		cout<<"\tmovl\t$1, %edi"<<endl;
		cout<<"\tmovl\t$0, %eax"<<endl;
		cout<<"\tcall\t__printf_chk@PLT"<<endl;
	}
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | Bloc
void Statement(void){
	if(strcmp(lexer->YYText(),"if")==0)
		ifStatement();
	else if(strcmp(lexer->YYText(),"while")==0)
		whileStatement();
	else if(strcmp(lexer->YYText(),"for")==0)
		forStatement();
	else if(strcmp(lexer->YYText(),"begin")==0)
		beginStatement();
	else if(current == DISPLAY)
		displayStatement();
	else
		AssignementStatement();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





