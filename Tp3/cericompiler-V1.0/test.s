			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:	.string "%llu\n"
a:	.quad 0
b:	.quad 0
r:	.quad 0
t:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $89
	pop a
	push $85
	pop r
	push a
	pop %rdx
	movq $FormatString1, %rsi
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	push r
	pop %rdx
	movq $FormatString1, %rsi
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
