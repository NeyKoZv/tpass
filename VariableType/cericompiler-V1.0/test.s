			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:	.string "%llu\n"
a:	.quad 0
b:	.quad 0
c:	.quad 0
r:	.quad 0
t:	.quad 0
p:	.quad 0
o:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $2
	pop r
	push $0
	pop t
	push $0
	pop p
	push $0
	pop o
si1:
	push r
	push $2
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai2	# If equal
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	jnz if2
then1:
	push $8
	pop t
	jmp finif1
if2:
	push r
	push $4
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai3	# If equal
	push $0		# False
	jmp Suite3
Vrai3:	push $0xFFFFFFFFFFFFFFFF		# True
Suite3:
	jnz if3
then2:
	push $18
	pop t
	jmp finif1
if3:
	push $5
	pop t
finif1:
	push t
	pop %rdx
	movq $FormatString1, %rsi
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	push $4
	pop r
si4:
	push r
	push $2
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai5	# If equal
	push $0		# False
	jmp Suite5
Vrai5:	push $0xFFFFFFFFFFFFFFFF		# True
Suite5:
	jnz if5
then4:
	push $8
	pop t
	jmp finif4
if5:
	push r
	push $4
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai6	# If equal
	push $0		# False
	jmp Suite6
Vrai6:	push $0xFFFFFFFFFFFFFFFF		# True
Suite6:
	jnz if6
then5:
	push $18
	pop t
	jmp finif4
if6:
	push $5
	pop t
finif4:
	push t
	pop %rdx
	movq $FormatString1, %rsi
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	push $6
	pop r
si7:
	push r
	push $2
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai8	# If equal
	push $0		# False
	jmp Suite8
Vrai8:	push $0xFFFFFFFFFFFFFFFF		# True
Suite8:
	jnz if8
then7:
	push $8
	pop t
	jmp finif7
if8:
	push r
	push $4
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai9	# If equal
	push $0		# False
	jmp Suite9
Vrai9:	push $0xFFFFFFFFFFFFFFFF		# True
Suite9:
	jnz if9
then8:
	push $18
	pop t
	jmp finif7
if9:
	push $5
	pop t
finif7:
	push t
	pop %rdx
	movq $FormatString1, %rsi
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	push $0
	pop t
si10:
	push r
	push $2
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai11	# If equal
	push $0		# False
	jmp Suite11
Vrai11:	push $0xFFFFFFFFFFFFFFFF		# True
Suite11:
	jnz if11
then10:
	push $8
	pop t
	jmp finif10
if11:
	push r
	push $4
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai12	# If equal
	push $0		# False
	jmp Suite12
Vrai12:	push $0xFFFFFFFFFFFFFFFF		# True
Suite12:
	jnz if12
then11:
	push $18
	pop t
	jmp finif10
if12:
finif10:
	push t
	pop %rdx
	movq $FormatString1, %rsi
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
switch:
	push $5
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop 5
	push $8
	push 5
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jnz endcase14
do14:
	push $0
	pop t
endcase14:
finswitch:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
