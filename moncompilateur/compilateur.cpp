//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//  
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current;				// Current car	
char nexchar;				// Next car

void ReadChar(void){		// Read character and skip spaces until 
				// non space character is read
	current = nexchar;
	while(cin.get(nexchar) && (nexchar==' '||nexchar=='\t'||nexchar=='\n'));
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression] OK
// SimpleExpression := Term {AdditiveOperator Term} OK
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit} OK

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  OK 
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"


void AdditiveOperator(void){
	if(current=='+'||current=='-'){
		ReadChar();
	}
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}

void RelativeOperator(void){
	if(current=='='){
		if(nexchar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='<'){
		if(nexchar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='!'){
		if(nexchar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='>'){
		if(nexchar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else Error("Opérateur relatif attendu");
}

void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

void Nombre(void){
	string nombre = "";
	if(((current>='0' && current <='9'))&&((nexchar>='0' && nexchar <='9'))){
		while(current>='0' && current<='9'){
			nombre += current;
			ReadChar();
		}
		cout << "\tpush $" << nombre << endl;
	}
	else{
		Digit();
	}
	
}

void ArithmeticExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Nombre();
	     	else
			Error("'(' ou chiffre attendu");
}

void Expression(void){
	char relop1, relop2;
	ArithmeticExpression();
	if(current=='='||current=='>'||current=='<'){
		relop1 = current;
		relop2 = nexchar;
		RelativeOperator();
		ArithmeticExpression();
		cout<<"\tpop %rbx"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tcmp %rbx, %rax"<<endl;
		switch(relop1){
			case '=' :
				if(relop2=='='){
					cout<<"\tjnz False"<<endl;
				}
			break;
			case '>' :
				if(relop2=='='){
					cout<<"\tjnea False"<<endl;
				}
				else if(relop2=='('||(relop2>='0' && relop2 <='9')){
					cout<<"\tjna False"<<endl;
				}
			break;
			case '<' :
				if(relop2=='='){
					cout<<"\tjnbe False"<<endl;
				}
				else if(relop2=='('||(relop2>='0' && relop2 <='9')){
					cout<<"\tjnb False"<<endl;
				}
			break;
			case '!' :
				if(relop2=='='){
					cout<<"\tjz False"<<endl;
				}
			break;
		}
		//RETURN 1
		cout<<"True:"<<endl;
		cout<<"\tpush $0xFFFFFFFFFFFFFFFF"<<endl;
		cout<<"\tjmp End"<<endl;
		//RETURN 0
		cout<<"False:"<<endl;
		cout<<"\tpush $0"<<endl;
		cout<<"End:"<<endl;
	}
}

void ArithmeticExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();//Avancer la tete de lecture pour que next ait quelque chose
	ReadChar();//Le current à un caractere
	Expression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
